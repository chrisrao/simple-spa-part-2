import os
import flask


def build():
    app = flask.Flask(__name__)
    app.add_url_rule(
        rule="/assets/<path:path>",
        methods=["GET"],
        view_func=assets,
        provide_automatic_options=True,
    )
    app.add_url_rule(
        rule="/",
        methods=["GET"],
        view_func=landing,
        defaults={"path": ""},
        provide_automatic_options=True,
    )
    app.add_url_rule(
        rule="/<path:path>", methods=["GET"], view_func=landing, provide_automatic_options=True,
    )
    return app


def landing(path):
    return flask.render_template("landing.html")


def assets(path):
    return flask.send_from_directory('../ui-dist', path)
