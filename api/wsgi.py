import os

from server import server

if __name__ == '__main__':
    app = server.build()
    app.run(host="0.0.0.0", port=5000, debug=os.environ.get('DEV'))